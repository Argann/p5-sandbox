class Wave
{
    constructor(numberOfPoints, width, startY)
    {
        this.points = [];

        for (let i = 0; i <= numberOfPoints; i++) {
            this.points.push(new Point(i * (width / numberOfPoints), startY));
        }
    }

    applyValues(values)
    {
        for (let i = 0; i < values.length; i++)
        {
            this.points[i].move(values[i]);
        }
    }

    draw()
    {
        let previousPoint = null;

        this.points.forEach(point => {

            point.draw();

            if (previousPoint !== null)
            {
                strokeWeight(2);
                stroke("#FEFAD4")
                line(previousPoint.x, previousPoint.y, point.x, point.y);
            }

            previousPoint = point;
        });
    }
}