let MAX_AMPLITUDE = 100;
let MAX_AMPLITUDE_STEP = 20;

let NUMBER_OF_POINTS = 200;
let NUMBER_OF_WAVES = 3;

let values = [0];

// === UTILS ===

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function clamp(num, min, max) {
    return num <= min ? min : num >= max ? max : num;
}

// === METHODS ===

function pushNewValue()
{
    let previousValue = values[0];
    let offset = getRandomInt(MAX_AMPLITUDE_STEP) - (MAX_AMPLITUDE_STEP / 2);
    let newValue = clamp(previousValue + offset, -(MAX_AMPLITUDE/2), MAX_AMPLITUDE/2);

    console.log(newValue);

    values.unshift(newValue);

    if (values.length > NUMBER_OF_POINTS)
        values.pop();
}

// === P5 ===

function setup() 
{
    createCanvas(windowWidth, windowHeight);

    background("#765D69");

    for (let i = 0; i < NUMBER_OF_POINTS; i++) 
    {
        pushNewValue();
    }

    console.log(values);

    for (let i = 0; i < NUMBER_OF_WAVES; i++)
    {
        let wave = new Wave(NUMBER_OF_POINTS, width, i * (height / NUMBER_OF_WAVES) + (0.5 * (height / NUMBER_OF_WAVES)));
        
        wave.applyValues(values);

        wave.draw();
    }
}

function draw() 
{
}