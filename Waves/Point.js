class Point
{
    constructor(x, startY)
    {
        this.x = x;
        this.startY = startY;
        this.y = startY;
    }

    move(newY)
    {
        this.y = this.startY + newY;
    }

    draw()
    {
        ellipse(this.x, this.y, 3);
    }
}