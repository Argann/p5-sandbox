let BOX_SIZE = 10;
let OFFSET = 5;

let WIDTH = 5;
let HEIGHT = 30;

function setup() 
{
    createCanvas(windowWidth, windowHeight, WEBGL);
}

function draw() 
{
    background(255);
    smooth();

    translate(-width/2, 0, -140);

    for (let w = 0; w < WIDTH; w++) {

        push();

        for (let h = 0; h < HEIGHT; h++) {

            var t = millis() / 4000;

            var n = noise(t + w/20, t + h/10);

            translate(0, BOX_SIZE + OFFSET, n*10);

            box(BOX_SIZE);
        }

        pop();

        translate(BOX_SIZE + OFFSET, 0, 0);
    }
}