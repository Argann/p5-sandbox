class Propagation
{
    constructor (startPosition, color)
    {
        this.startPosition = startPosition;
        this.color = color;
        this.radius = 0;
        this.speed = 1;
    }

    tick()
    {
        this.radius += 50;
    }

    draw()
    {
        fill(this.color);
        circle(this.startPosition.x, this.startPosition.y, this.radius);
    }
}

let propagations = [];
let colors = [
    "#F44174",
    "#4C1E4F",
    "#B5A886",
    "#FEE1C7",
    "#FA7E61",
]

function setup() 
{
    createCanvas(windowWidth, windowHeight);
    noStroke();
    background(colors[Math.floor(Math.random() * colors.length)]);
}

function draw() 
{
    propagations.forEach(prop => {
        prop.tick();
        prop.draw();
    });
}

function mousePressed()
{
    var color = colors[Math.floor(Math.random() * colors.length)];
    propagations.push(new Propagation(createVector(mouseX, mouseY), color));
}