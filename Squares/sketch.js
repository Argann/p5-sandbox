let LINE_HEIGHT = 15;
let BLOCK_MAX_WIDTH = 120;
let BLOCK_MIN_WIDTH = 60;
let VERTICAL_OFFSET = 5;
let HORIZONTAL_OFFSET = 5;

function randomIntFromInterval(min, max) 
{ 
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function randomColor()
{
    return color(random(0, 360), random(20, 80), 100);
}

function clamp(num, min, max)
{
    return num <= min ? min : num >= max ? max : num;
}

function setup() 
{
    createCanvas(windowWidth, windowHeight);

    colorMode(HSB);

    background(0);
    noStroke();

    var cursorX = -100;
    var cursorY = 0;

    while (cursorY < height)
    {
        while (cursorX < width)
        {
            var newX = randomIntFromInterval(BLOCK_MIN_WIDTH, BLOCK_MAX_WIDTH);
            newX = Math.min(width - cursorX, newX);

            fill(randomColor());
            rect(cursorX, cursorY, newX, LINE_HEIGHT, 4, 4, 4, 4);
            cursorX += newX + HORIZONTAL_OFFSET;
        }
        cursorY += LINE_HEIGHT + VERTICAL_OFFSET;
        cursorX = -20;
    }
}

function draw() 
{
    
}