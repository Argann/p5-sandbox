let mySound;
let fft;
let previousFrameSnared = false;
let timer = 0;
let maxTimer = 150;
let numberOfClaps = 0;

function preload()
{
    soundFormats('mp3');
    mySound = loadSound("basic2.mp3", playMusic);
}

function playMusic()
{
    mySound.loop();
    mySound.play();
}

function pauseMusic()
{
    mySound.stop();
}

function clickCanvas()
{
    if (mySound.isPlaying())
    {
        pauseMusic();
    }
    else
    {
        playMusic();
    }
}

function setup() 
{
    let cnv = createCanvas(windowWidth, windowHeight);
    cnv.mousePressed(clickCanvas);

    fft = new p5.FFT();

    coucou = color(0);
}

function clap()
{
    previousFrameSnared = true;
    timer = maxTimer;
    numberOfClaps++;
}

function draw()
{
    translate(width/2, height/2);

    fft.analyze();

    background(coucou);
    noStroke();

    let bass = Math.max(0, (fft.getEnergy(65)-216)*7);
    let snare = Math.max(0, ((fft.getEnergy(4000)*1.5)-170)*3);
    let hihat = fft.getEnergy(12000)*2;

    if (!previousFrameSnared && snare > 150)
    {
        clap();
    }
    else if (previousFrameSnared && snare == 0)
    {
        previousFrameSnared = false;
    }

    if (timer > 0)
    {
        timer = Math.max(0, timer - deltaTime);
    }

    for (let i = -10; i <= 10; i++)
    {
        for (let j = -10; j <= 10; j++)
        {

            let isColOdd = Math.abs(i) % 2 == 1 ? 1 : -1;
            let isRowOdd = Math.abs(j) % 2 == 1 ? 1 : -1;

            let direction = numberOfClaps % 2 == 1 ? -isColOdd : isColOdd;

            let offsetY = direction * (timer * 1.5);

            let offsetX = offsetY == 0 ? isRowOdd * (millis() * 0.1) % 100 : 0;

            fill(color(100 + (hihat / 2)));
            circle((i*100) + offsetX, (j*100)+offsetY, (bass/6) + 20);

            fill(color(50 + (hihat / 2)));
            circle((i*100) + offsetX, (j*100)+offsetY, (bass/6) + 15);
        }
    }

    
}