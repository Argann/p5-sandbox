class RigidbodyCircle
{
    constructor(sketch)
    {
        this.sketch = sketch;

        this.x = 0;
        this.y = 0;

        this.mass = 10;

        // in pixels/second
        this.velocityX = 0;
        this.velocityY = 0;
    }

    setPosition(pos)
    {
        this.x = pos.x;
        this.y = pos.y;
    }

    setMass(newMass)
    {
        this.mass = newMass;
    }

    setVelocity(vel)
    {
        this.velocityX = vel.x;
        this.velocityY = vel.y;
    }

    addForce(force)
    {
        this.velocityX += force.x;
        this.velocityY += force.y;
    }

    // called 30 times per second
    fixedUpdate()
    {
        this.velocityY += (980 / 30);

        this.x += this.velocityX / 30; 
        this.y += this.velocityY / 30;

        if (this.y >= this.sketch.height - 20)
        {
            this.y = this.sketch.height - 20;
            this.velocityY = 0;
        }
        else if (this.y <= 20)
        {
            this.y = 20;
            this.velocityY = 0;
        }
    }

    draw()
    {
        this.sketch.circle(this.x, this.y, 10);
    }
}