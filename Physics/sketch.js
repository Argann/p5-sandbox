const s = ( sketch ) => 
{
    let circle = new RigidbodyCircle(sketch);

    sketch.setup = () => 
    {
        sketch.createCanvas(sketch.windowWidth, sketch.windowHeight);

        sketch.frameRate(30);

        circle.setPosition(sketch.createVector(50, 50));
    }
    
    sketch.draw = () => 
    {
        sketch.background(0);

        circle.fixedUpdate();
        circle.draw();
    }

    sketch.mousePressed = () =>
    {
        circle.addForce(sketch.createVector(20, -500));
        console.log("Hello");
    }
}

let myp5 = new p5(s);